package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        int sex = id.charAt(9);
        Sex sexreturn = sex % 2 == 0 ? Sex.WOMAN : Sex.MAN;
        return Optional.of(sexreturn);
    }

    @Override
    public boolean isCorrect() {
        //9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
        int controlsum = (9*Character.getNumericValue(id.charAt(0)))
                +(7*Character.getNumericValue(id.charAt(1)))
                +(3*Character.getNumericValue(id.charAt(2)))
                +(Character.getNumericValue(id.charAt(3)))
                +(9*Character.getNumericValue(id.charAt(4)))
                +(7*Character.getNumericValue(id.charAt(5)))
                +(3*Character.getNumericValue(id.charAt(6)))
                +(Character.getNumericValue(id.charAt(7)))
                +(9*Character.getNumericValue(id.charAt(8)))
                +(7*Character.getNumericValue(id.charAt(9)));
        int rest = controlsum % 10;
        System.out.println(controlsum);
        System.out.println(rest);
        return rest == Character.getNumericValue(id.charAt(10));
    }

    @Override
    public Optional<String> getDate() {
        String day = id.substring(4,6);
        int i = Integer.parseInt(id.substring(2,4));
        String year = id.substring(0,2);
        String month = "";
        if(i>=81 && i<=92){
            month = parsemonth(i - 80);
            return Optional.of(day+"-"+ month +"-"+"18"+year);
        } else if(i>=1&&i<=9){
            month = parsemonth(i);
            return Optional.of(day+"-"+ month +"-"+"19"+year);
        } else if(i>=21&&i<=32){
            month = parsemonth(i - 20);
            return Optional.of(day+"-"+ month +"-"+"20"+year);
        } else if(i>=41&&i<=52){
            month = parsemonth(i - 40);
            return Optional.of(day+"-"+ month +"-"+"21"+year);
        } else if(i>=61&&i<=72){
            month = parsemonth(i - 60);
            return Optional.of(day+"-"+ month +"-"+"22"+year);
        }
        return Optional.empty();
    }

    private String parsemonth(int i) {
        String month = String.valueOf(i);
        if(month.length()<2){
            month = "0" + month;
            return month;
        }
        return month;
    }
}
