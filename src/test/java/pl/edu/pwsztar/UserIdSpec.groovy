package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given:
        def userid = new UserId(id);
        when:
        def isvalid = userid.isCorrectSize();
        then:
        isvalid == expected;
        where:
        id            || expected
        "98060405174" || true
        "68100705021" || true
        "6212140"     || false
        ""            || false
    }

    @Unroll
    def "should get sex"(){
        given:
        def userId = new UserId(id);
        when:
        def sex = userId.getSex();
        then:
        sex.get() == expected;
        where:
        id || expected
        "98060405174" || UserIdChecker.Sex.MAN
        "68100705021" || UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should check if number is valid"(){
        given:
        def userid = new UserId(id);
        when:
        def isvalid = userid.isCorrect();
        then:
        isvalid == expected;
        where:
        id            || expected
        "68100705021" || true
        "98060405174" || true
        "95101578331" || false
    }

    @Unroll
    def "should return birth date based on pesel"(){
        given:
        def userid = new UserId(id);
        when:
        def date = userid.getDate();
        then:
        date.get() == expected;
        where:
        id            ||  expected
        "98060405174" || "04-06-1998"
        "98860405174" || "04-06-1898"
        "98260405174" || "18-08-2098"
    }
}
